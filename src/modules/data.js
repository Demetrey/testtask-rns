// Requires only for the Custom Template
const links = {
  github: "https://gitlab.com/Demetrey",
  behance: "https://www.behance.net/demetory",
  telegram: "https://t.me/Demetory",
  cv: "https://demetrey.ru/download/CV-Demetrey-2020-EN.pdf",
  hh: "https://novosibirsk.hh.ru/resume/215651d0ff090383cf0039ed1f71476f4f5858",
  dd: "https://demetrey.ru/",
};

export { links };
