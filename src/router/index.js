// Imports
import { createRouter, createWebHistory } from "vue-router";

// Home
import Home from "../views/Home.vue";

// Routes
const routes = [
  // Root
  { path: "/:pathMatch(.*)*", name: "NotFound", component: () => import("../views/NotFound.vue") },
  { path: "/", name: "Home", component: Home },
];

// Create Router
export const router = createRouter({
  scrollBehavior(to) {
    if (to.hash) {
      return { el: to.hash, behavior: "smooth" };
    } else {
      return { top: 0 };
    }
  },
  history: createWebHistory(),
  routes,
});
