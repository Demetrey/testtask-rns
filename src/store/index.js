import { resolveComponent } from "vue";
import { createStore } from "vuex";

export const store = createStore({
  state() {
    return {
      // <Array> Required. List of formulas.
      formulas: ["(x+y)*5", "(x+y)*7"],
      // <Array> Required. List of possible actions.
      actions: ["delete", "edit"],
      // <Array> Required. List of Variables.
      variables: ["Число", "X", "Y", "Z"],
      // <Array> Required. List of Math Operations.
      operands: ["+", "-", "*", "/", "(", ")"],
    };
  },

  mutations: {
    // Deletes the Formula from the List
    FORMULA_DELETE(state, payload) {
      state.formulas.splice(payload, 1);
    },

    // Adds Formula to the List
    FORMULA_ADD(state, payload) {
      state.formulas.push(payload);
    },

    // Updates Formula in the List
    FORMULA_UPDATE(state, payload) {
      state.formulas[payload.index] = payload.formula;
    },
  },

  getters: {
    isFormulaExists: (state) => (payload) => {
      return state.formulas.includes(payload);
    },
  },

  actions: {
    // Updates Formula in the List
    updateFormula({ commit }, payload) {
      return new Promise((resolve) => {
        commit("FORMULA_UPDATE", payload);
        resolve("success");
      });
    },

    // Adds Formula to the List
    addFormula({ commit, state, getters }, payload) {
      return new Promise((resolve, reject) => {
        const isExist = getters.isFormulaExists(payload);
        if (isExist) {
          reject("exists");
        } else {
          commit("FORMULA_ADD", payload);
          resolve("success");
        }
      });
    },

    // Deletes Formula from the List
    formulaDelete({ commit, state }, payload) {
      let formulas = state.formulas.filter((item) => item !== payload);
      commit("FORMULA_DELETE", formulas);
    },
  },
});

export default store;
